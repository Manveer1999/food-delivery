import 'package:flutter/material.dart';
import 'package:food_delivery/views/SideMenu.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:toast/toast.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin{

  int selectedItemIndex = 0;
  List<bool> isSelected = List<bool>();
  AnimationController controller;
  Animation<Offset> slideAnimation;
  bool isDrawerOpen = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    makeListEmpty();
    controller = AnimationController(vsync: this, duration: Duration(seconds: 1));
    slideAnimation = Tween<Offset>(begin: Offset(100,100), end: Offset(0,0)).animate(controller);
  }

  Future<void> makeListEmpty() async{
    for(int i=0; i<10; i++) {
      isSelected.add(false);
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SafeArea(
        child: Builder(
          builder: (context){
            return Stack(
              children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    color: Colors.black,
                    height: 50,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              "Cart",
                              style: GoogleFonts.comfortaa(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Row(
                              children: [
                                Container(
                                  height: 40,
                                  width: 40,
                                  decoration: BoxDecoration(
                                    color: Colors.grey.shade200,
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.all(6),
                                      child: Image.asset(
                                        "assets/icons/pizzaImage.PNG",
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(width: 5,),
                                Container(
                                  height: 40,
                                  width: 40,
                                  decoration: BoxDecoration(
                                    color: Colors.grey.shade200,
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.all(6),
                                      child: Image.asset(
                                        "assets/icons/pizzaImage.PNG",
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  height: 40,
                                  width: 40,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    color: Colors.deepOrange.shade100,
                                  ),
                                  child: Center(
                                    child: Text("1",
                                      style: GoogleFonts.comfortaa(
                                          fontWeight: FontWeight.w900
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(width: 5,),
                                Image.asset(
                                  "assets/icons/Detail-arrow.png",
                                  height: 40,
                                  width: 40,

                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height - 80,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(25), bottomRight: Radius.circular(25))
                  ),
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                InkWell(
                                  child: Image.asset(
                                    "assets/icons/menu.png",
                                    width: 25,
                                    height: 15,
                                    fit: BoxFit.contain,
                                  ),
                                  onTap: (){
                                    Scaffold.of(context).openDrawer();
                                  },
                                ),
                                Row(
                                  children: [
                                    Image.asset(
                                      "assets/icons/search.png",
                                      height: 18,
                                      width: 18,
                                      fit: BoxFit.contain,
                                    ),
                                    SizedBox(width: 15,),
                                    CircleAvatar(
                                      radius: 20,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(20),
                                          color: Colors.black,
                                        ),
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                          SizedBox(height: 10,),
                          Text(
                            "Food",
                            style: GoogleFonts.comfortaa(
                                fontSize: 35,
                                fontWeight: FontWeight.w900
                            ),
                          ),
                          Text(
                            "Delivery",
                            style: GoogleFonts.comfortaa(
                                fontSize: 40,
                                color: Colors.grey.shade700
                            ),
                          ),
                          SizedBox(height: 15,),
                          Container(
                            height: 110,
                            child: ListView.builder(
                              itemCount: 10,
                              shrinkWrap: true,
                              physics: ClampingScrollPhysics(),
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index){
                                return category("assets/icons/burger.png", "Burger", index);
                              },
                            ),
                          ),
                          SizedBox(height: 15,),
                          Text(
                            "Popular",
                            style: GoogleFonts.comfortaa(
                                fontSize: 15,
                                fontWeight: FontWeight.w600
                            ),
                          ),
                          SizedBox(height: 5,),
                          Container(
                            height: 230,
                            child: ListView.builder(
                              itemCount: 10,
                              shrinkWrap: true,
                              physics: ClampingScrollPhysics(),
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index){
                                return popularItemCard();
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                // AnimatedBuilder(
                //   animation: slideAnimation,
                //   builder: (context,b){
                //     return GestureDetector(
                //         onTap: (){
                //           setState(() {
                //             isDrawerOpen = !isDrawerOpen;
                //             Toast.show(isDrawerOpen.toString(), context);
                //           });
                //           isDrawerOpen ? controller.forward() : controller.reverse();
                //         },
                //         child: SideMenu());
                //   },
                // ),
              ],
            );
          },
        ),
      ),
      drawer: Container(
        child: SideMenu()
      ),
    );
  }

  closeDrawer(BuildContext context){
    Scaffold.of(context).openEndDrawer();
  }

  Widget popularItemCard() {
    return Card(
      color: Colors.grey.shade200,
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: Container(
        child: Padding(
          padding: const EdgeInsets.all(22),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset(
                "assets/icons/pizzaImage.PNG",
                height: 110,
                width: 110,
                fit: BoxFit.contain,
              ),
              SizedBox(height: 15,),
              Text(
                "\$12.99",
                style: GoogleFonts.comfortaa(
                    fontSize: 24,
                    fontWeight: FontWeight.w800
                ),
              ),
              SizedBox(height: 5,),
              Text(
                "Quattro formaggi",
                style: GoogleFonts.comfortaa(
                    fontSize: 12,
                    fontWeight: FontWeight.w900
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }

  Widget category(String imagePath, String name, int index){

    return InkWell(
      onTap: (){
        setState(() {
          isSelected[selectedItemIndex] = false;
          selectedItemIndex = index;
          isSelected[index] = !isSelected[index];
        });
      },
      child: Container(
        margin: EdgeInsets.all(5),
        child: Stack(
          children: [
            Center(
              child: Image.asset(
                isSelected[index] ? "assets/icons/active.png" :"assets/icons/un_active.png",
                height: 110,
                width: 70,
              ),
            ),
            Container(
              height: 110,
              width: 70,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      imagePath,
                      height: 23,
                      width: 23,
                      fit: BoxFit.contain,
                    ),
                    SizedBox(height: 10,),
                    Text(
                      "Burger",
                      style: GoogleFonts.comfortaa(
                          fontSize: 11,
                          fontWeight: FontWeight.w900
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

}
