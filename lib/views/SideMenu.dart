import 'package:flutter/material.dart';
import 'package:food_delivery/views/HomeScreen.dart';
import 'package:google_fonts/google_fonts.dart';

class SideMenu extends StatefulWidget {
  @override
  _SideMenuState createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                        child: Image.asset("assets/sideMenuIcons/cancel.png",
                          height: 20,
                          width: 20,
                          fit: BoxFit.contain,
                        ),
                        onTap: (){
                          HomeScreen().createState().closeDrawer(context);
                        },
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        height: 70,
                        width: 70,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(35),
                          color: Colors.black
                        ),
                        child: Icon(Icons.account_circle,size: 70,),
                      ),
                      SizedBox(width: 15,),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Johnson Angelina",
                            style: GoogleFonts.comfortaa(
                              fontSize: 18,
                              fontWeight: FontWeight.w900,
                            ),
                          ),
                          SizedBox(height: 8,),
                          Text(
                            "Johnson7878@gmail.com",
                            style: GoogleFonts.comfortaa(
                              fontSize: 11,
                              fontWeight: FontWeight.w800,
                              color: Colors.grey.shade700,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 30,),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: ListTile(
                      title: Text(
                        "  Menu List",
                        style: GoogleFonts.comfortaa(
                            fontWeight: FontWeight.w900,
                            fontSize: 14,
                        ),
                      ),
                      leading: Image.asset(
                        "assets/sideMenuIcons/1.png",
                        height: 25,
                        width: 25,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: ListTile(
                      title: Text(
                        "  My Cart",
                        style: GoogleFonts.comfortaa(
                            fontWeight: FontWeight.w900,
                            fontSize: 14
                        ),
                      ),
                      leading: Image.asset(
                        "assets/sideMenuIcons/2.png",
                        height: 25,
                        width: 25,
                        fit: BoxFit.contain,
                      ),
                      trailing: Container(
                        height: 25,
                        width: 25,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(13),
                          color: Colors.deepOrange.shade100
                        ),
                        child: Center(
                          child: Text(
                            "5",
                            style: GoogleFonts.comfortaa(
                                fontSize: 12,
                              fontWeight: FontWeight.bold,
                            ),),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: ListTile(
                      title: Text(
                        "  Book Order",
                        style: GoogleFonts.comfortaa(
                            fontWeight: FontWeight.w900,
                            fontSize: 14
                        ),
                      ),
                      leading: Image.asset(
                        "assets/sideMenuIcons/3.png",
                        height: 25,
                        width: 25,
                        fit: BoxFit.contain,
                      ),
                      trailing: Container(
                        height: 25,
                        width: 25,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(13),
                          color: Colors.deepOrange.shade100
                        ),
                        child: Center(
                          child: Text(
                            "5",
                            style: GoogleFonts.comfortaa(
                                fontSize: 12,
                              fontWeight: FontWeight.bold,
                            ),),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: ListTile(
                      title: Text(
                        "  Favourite",
                        style: GoogleFonts.comfortaa(
                            fontWeight: FontWeight.w900,
                            fontSize: 14
                        ),
                      ),
                      leading: Image.asset(
                        "assets/sideMenuIcons/4.png",
                        height: 25,
                        width: 25,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: ListTile(
                      title: Text(
                        "  About us",
                        style: GoogleFonts.comfortaa(
                            fontWeight: FontWeight.w900,
                            fontSize: 14
                        ),
                      ),
                      leading: Image.asset(
                        "assets/sideMenuIcons/5.png",
                        height: 25,
                        width: 25,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: ListTile(
                      title: Text(
                        "  Our Policy",
                        style: GoogleFonts.comfortaa(
                            fontWeight: FontWeight.w900,
                          fontSize: 14
                        ),
                      ),
                      leading: Image.asset(
                        "assets/sideMenuIcons/6.png",
                        height: 25,
                        width: 25,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),

                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: 65,
                      width: 65,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(35),
                        border: Border.all(color: Colors.grey)
                      ),
                      child: Center(
                        child: Image.asset(
                          "assets/sideMenuIcons/login.png",
                          height: 30,
                          width: 30,
                          fit: BoxFit.contain,

                        ),
                      ),
                    ),
                    SizedBox(width: 15,),
                    Text(
                      "Login as delivery boy",
                      style: GoogleFonts.comfortaa(
                        fontWeight: FontWeight.w900,
                        fontSize: 14,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
