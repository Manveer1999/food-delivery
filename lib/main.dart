import 'package:flutter/material.dart';
import 'package:food_delivery/views/HomeScreen.dart';

void main() {
  runApp(
    MaterialApp(
      home: HomeScreen(),
    ),
  );
}
